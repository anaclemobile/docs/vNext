/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'AnacleMobile',
  tagline: 'AnacleMobile Documentation',
  url: 'https://anaclemobile.gitlab.io/',
  baseUrl: '/docs/vNext/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/anacle.png',
  organizationName: 'anaclesys', // Usually your GitHub org/user name.
  projectName: 'docs/vNext', // Usually your repo name.
  themeConfig: {
    sidebarCollapsible: false,
    navbar: {
      title: 'AnacleMobile vNext',
      logo: {
        alt: 'AnacleMobile',
        src: 'img/anacle.png',
      },
      items: [
        {
          type: 'doc',
          docId: 'training/intro',
          position: 'left',
          label: 'Training',
        },
        {
          to: 'features',
          position: 'left',
          label: 'Features',
        },
        {
          position: 'left',
          label: 'Support',
        },
         {
          position: 'left',
          label: 'More',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Training',
          items: [
            {
              label: 'Basic',
              to: 'training',
            },
            {
              label: 'Advanced',
              to: 'training/advanced/custom-renderers',
            },
          ],
        },
        {
          title: 'Features',
          items: [
            {
              label: 'Overview',
              to: 'features',
            },
            {
              label: 'Productivity',
              to: 'under-construction',
            },
            {
              label: 'Security',
              to: 'under-construction',
            },
            {
              label: 'Roadmap',
              to: 'under-construction',
            },
          ],
        },
        {
          title: 'Support',
          items: [
            {
              label: 'Code Snippets',
              to: 'under-construction',
            },
            {
              label: 'Knowledge Base',
              to: 'under-construction',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              html: '🚧 Under Construction'
            },
          ],
        },
      ],
      copyright: `Copyright © 2021 Anacle Systems Limited. Prepared by Zhen Zhi.`,
    },
    prism: {
      additionalLanguages: ['csharp'],
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // editUrl:
          //   'https://github.com/facebook/docusaurus/edit/master/website/',
          routeBasePath: '/',
          // sidebarItemsGenerator: async function ({
          //   defaultSidebarItemsGenerator,
          //   ...args
          // }) {
          //   /** @type {any[]} */
          //   let sidebarItems = await defaultSidebarItemsGenerator(args);

          //   // Exclude under-construction docs
          //   //
          //   sidebarItems = sidebarItems.filter(i => !i.id || (i.id && !i.id.includes('features/under-construction')));

          //   return sidebarItems;
          // },
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
