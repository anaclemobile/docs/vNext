---
sidebar_position: 1
slug: /
---

# AnacleMobile vNext

Welcome to the one-stop documentation site for AnacleMobile! This site will be the new home for all things related to mobile apps and more specifically, AnacleMobile.

This site is catered for the **next version** of AnacleMobile. For the current version of AnacleMobile, please click [here](https://anaclemobile.gitlab.io/docs/vCurrent/).

## Training

For engineers learning to create mobile apps with AnacleMobile, click [here](training/intro.md) to start learning the ropes.

## Features

AnacleMobile is a set of reusable components built on top of Xamarin.Forms. [Checkout](features/01-overview.md) the supported features implemented by AnacleMobile vNext.

## Support

As a one-stop documentation site, we will be sharing useful code snippets and know-hows from time to time. Click [here](under-construction.md) to check it out.

## More

🚧 Under construction
