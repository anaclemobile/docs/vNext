# Gotchas

## Do not use finalizers

```csharp
// Example usage of a finalizer (or destructor).
//
~WorkPage()
{
    if (workViewModel is null)
    {
        return;
    }

    workViewModel.WorkDetailLoaded -= workViewModel_WorkDetailLoaded;
}
```

In C#, using a finalizer (or a destructor) is usually not recommended as there is no guarantee that it will ever be called. A finalizer will only be called when the garbage collector collects the object.

Instead, if there are unmanaged resources or cleanup operations to be executed, we should use the Dispose pattern: <https://docs.microsoft.com/en-us/dotnet/standard/garbage-collection/implementing-dispose>.

```csharp
// Example usage of the dispose pattern.
// 
public static byte[] Decompress(this byte[] b)
{
    if (b == null) return null;
    using (var ims = new MemoryStream(b))
    {
        using (var oms = new MemoryStream())
        {
            using (var gzs = new GZipStream(ims, CompressionMode.Decompress))
            {
                gzs.CopyTo(oms);
            }
            
            var decompressed = oms.ToArray();
            return decompressed;
        }
    }
}
```

## Avoid using asynchronous methods in getters, setters and contructors

```csharp
// Calling an asynchronous method in a setter.
//
public string ObjectId
{
    set
    {
        Guid.TryParse(value, out var workId);
        _ = workViewModel.GetWorkDetailAsync(workId);
    }
}
```

Calling an async method as above is not recommended as any exception from the async method will not be bubbled up from the stack.

In general, it is better to avoid calling an async method in any getters, setters and constructors. We should use the equivalent lifecycle or event handler method for any initialization logic that needs to be executed.

## Handle exceptions from the caller as much as possible

Rethrow exceptions
