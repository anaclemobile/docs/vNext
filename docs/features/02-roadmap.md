# Roadmap

## Q4 2021

* v4.0.0 base implementation completion with basic infrastructure components in place (improved productivity, error handling & logging, and more) -- see [Overview](01-overview.md)
* Web API enhancement
  * RBAC enabled endpoints
  * Model validation
  * Conditional object mapping

## Q1 2022

* Training modules for v4.0.0
* Migrate Web API to ASP.NET Core (with .NET 5/6)
* Continue work on v4.0.0
* v4.0.0 upgrade to [.NET MAUI](https://github.com/dotnet/maui) (v5.0.0)
  * In preparation for Android 13+ & iOS 16+
  * Better tooling with VS 2022 (hot reload)
* Explore: Single endpoint API (GraphQL-like or just use GraphQL)
* Prototype MVP with [Flutter](https://flutter.dev/) (v6.0.0)

## Q2 2022

* v6.0.0 Flutter **feature parity** with Xamarin.Forms and .NET MAUI
* Use Flutter for real life project
* Training modules for v6.0.0 with Flutter

*Last update: 2021-11-24*

