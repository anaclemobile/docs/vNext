---
slug: /features
---

# Overview

## Goals of v4.0.0

1. Improved developer productivity: do more with less code, auto generated API client
2. Fulfil common requirements: local database, exception handling, standard UI components, security features, push notifications
3. Plug and play core features and additional features
4. Standard codebase and encouraging best practices
5. Well documented and available for self-help online

## Core features

Core features that every well-architected mobile app should have. These features **are portable**, i.e. you can use it for any projects with little to no code change (albeit with some configuration and setup).

| Status | Feature                                 | Remarks                                                                                                                                                                                                  |
| :----: | --------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|   ✅   | Auto generated API client from Web API  | Uses NSwag API generator.                                                                                                                                                                                |
|   ✅   | HTTP request auto retry                 | Auto retry when HTTP call encounters transient faults (network related or timed out).                                                                                                                    |
|   ✅   | Exception & crash logging               | *2021-06-28* Added support for saving exceptions/crashes into local DB.<br />*2021-10-24* Write exceptions and crash logs into local file system instead.                                            |
|   ✅   | HTTP Authorization header support       |                                                                                                                                                                                                          |
|   ✅   | Track ongoing HTTP requests on the page | Used for cancellation of background Task(s) on user logout.                                                                                                                                              |
|   ✅   | HTTP request idempotency                | Every HTTP requests will have an additional header `X-App-Http-Request-ID` that will be used as the cache key to ensure request/response idempotency.                                                    |
|   ✅   | Local database: master DB & user DB     | *2021-06-28* In progress. Used EF Core for local DB (master DB and user DB).<br />*2021-11-01* Dropped EF Core due to poor performance. Used a customized version of sqlite-net with Fluent API support. |
|   ✅   | Shared POCO classes                     | *2021-06-28* Added shared POCO class library with local DB fluent API configuration provided by EF Core.<br />*2021-11-01* Dropped EF Core due to poor performance.                                  |
|   🚧   | UI field validation handling            | Validation logic will be shared with Web API.                                                                                                                                                            |
|   ✅   | Snapshot-based download/search API      | Replaces master data used by previous iterations of AnacleMobile.                                                                                                                                        |
|   ✅   | HTTP response compression               | Uses IIS dynamic compression (Accept-Encoding header).                                                                                                                                                   |
|   🚧   | HTTP request/response logging           | *2021-11-09* Implemented as OWIN middleware. Requires more work to support logging request/response body.                                                                                              |
|   ✅   | Device ID tracking                      | A random GUID will be generated and persisted during the first run of the app to identify a particular device.                                                                                           |
|   ✅   | MVVM micro framework                    |                                                                                                                                                                                                          |
|   📦   | Source generators for ApiClientFactory  |                                                                                                                                                                                                          |
|   📦   | Source generators for MVVM mapping      |                                                                                                                                                                                                          |
|   📦   | Task cancellation framework             | Need a better way of handling `IsBusy` flag and task cancellations.                                                                                                                                      |
|   ✅   | App Center integration                  |                                                                                                                                                                                                          |
|   ✅   | Configurable object mapping             | Ignore properties with a configuration class. Customize mapping per direction.                                                                                                                           |

## UI components

Standard UI components.

| Status | Feature                                           | Remarks                                         |
| :----: | ------------------------------------------------- | ----------------------------------------------- |
|   ✅   | Font Awesome 5 & 6 icons                          | Supports Font Awesome Pro.                      |
|   ✅   | Material icons                                    |                                                 |
|   ✅   | App bar                                           | Supports up to 4 action buttons.                |
|   ✅   | Bottom tab view                                   |                                                 |
|   ✅   | Picker                                            | Single value select, hierarchical value select. |
|   ✅   | Fields                                            | Date & time, text box, search text box.         |
|   🚧   | Error message, hint message, mandatory field icon |                                                 |
|   🚧   | AutomationID for automated tests                  | Partially implemented for some components.      |
|   🚧   | Good looking UI                                   |                                                 |

## Security features

Common security features.

| Status | Feature                                     | Remarks                                                                                                                                     |
| :----: | ------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- |
|   ✅   | Root/jailbreak detection                    |                                                                                                                                             |
|   ✅   | Local database encryption                   | Implemented with SQLCipher.                                                                                                                 |
|   ✅   | JWT-based authentication                    | Uses both access token and refresh token for authentication purposes.                                                                       |
|   ✅   | RBAC enabled API endpoints                  | Uses Function table for API endpoint RBAC.                                                                                                  |
|   ✅   | API endpoint model validation               | Uses validator classes implemented with [Fluent Validation](https://fluentvalidation.net/). Validator classes are shared with AnacleMobile. |
|   ✅   | SSL certificate pinning                     |                                                                                                                                             |
|   🚧   | Mutual TLS authentication                   |                                                                                                                                             |
|   ✅   | Secured API endpoint with subscription keys |                                                                                                                                             |
|   ✅   | JSON payload anti-XSS                       | HTML strings will be escaped by default.                                                                                                    |
|   ❓   | JSON payload sanitization                   |                                                                                                                                             |
|   🚧   | Role-based object mapping                   |                                                                                                                                             |
|   ✅   | Secure HTTP headers                         | Standard secure HTTP headers.                                                                                                               |
|   🚧   | RBAC for client side                        | Will be implemented together with offline mode.                                                                                             |
| ✅ | Encrypted API payload | *2021-12-23* Allows for encrypted communication when sending data from mobile app to web API. |
| ✅ | Disabled screenshot & hidden app screen in the app switcher | *2021-12-23* **Android only **Taking screenshots will not be allowed (using Android secure flag). On iOS, the native platform does not support disabling screenshots. |
| ❌ | C# code obfuscation | Not required because AOT is enabled for both Android and iOS.<br />Reference: https://devblogs.microsoft.com/premier-developer/should-you-obfuscate-your-xamarin-app/ |
| ❓ | Native code obfuscation | Android: Enabled with ProGuard.<br />iOS: No code obfuscation solution exists for now. Obfuscating iOS code will risk rejection from Apple App Store. Reference: https://developer.apple.com/app-store/review/guidelines/#performance |

## Authentication methods

Supported authentication methods. Do note that mobile apps can only support OIDC protocol. The ID token from the identity provider (IdP) will be used to exchanged with access tokens and refresh tokens from Simplicity API.

| Status | Feature                          | Remarks                                                               |
| :----: | -------------------------------- | --------------------------------------------------------------------- |
|   ✅   | Simplicity username and password | Access token and refresh token will be generated upon authentication. |
|   ✅   | Azure Active Directory (AAD)     | Supports OIDC authorization code flow only.                           |
|   📦   | Generic OIDC support                            | Supports OIDC authorization code flow (with PKCE) only. Can be used for AD FS or any IdP that supports authentication with OIDC.                          |
|   📦   | Google Sign In                   |                                                                       |
|   📦   | Facebook Sign In                 |                                                                       |
| 📦 | Firebase Sign In | |

\* OIDC authorization code flow with PKCE can be supported with customizations.

## Additional features

Advanced mobile features. These features **are portable** as well.

| Status | Feature                                                  | Remarks                                                                              |
| :----: | -------------------------------------------------------- | ------------------------------------------------------------------------------------ |
|   🚧   | Offline mode                                             |                                                                                      |
|   🚧   | Push notification: Azure Notification Hub                |                                                                                      |
|   🚧   | Push notification: Firebase Cloud Messaging (FCM) Direct |                                                                                      |
|   📦   | Bluetooth Low Energy (BLE)                               |                                                                                      |
|   📦   | Near-Field Communication (NFC)                           |                                                                                      |
|   📦   | Offline LeafletJS map                                    | Using QGIS and storing basemap as plain image files on device (~5 GB size required). |

## Simplicity 8.1 modules

The following features are based on our standard Simplicity v8.1 product modules. They **are not portable** and may require a rewrite for customized projects.

| Status | Module    | Remarks                              |
| :----: | --------- | ------------------------------------ |
|   ✅   | Work      | Based on standard Simplicity module. |
|   ✅   | Case      | Based on standard Simplicity module. |
|   🚧   | Equipment | Based on standard Simplicity module. |

## Legend

| Symbol | Description   |
| :----: | ------------- |
|   ✅   | Supported     |
|   ❌   | Not Supported |
|   🚧   | Implementing  |
|   📦   | Backlog       |
|   ❓   | Evaluating    |

*Last update: 2021-12-23*
